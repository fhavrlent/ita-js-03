import React, { Component } from 'react';
import Add from '../components/Add/Add';

class AddPage extends Component {

  render() {
    return (
      <div>
        <Add />
      </div>
    );
  }
}

export default AddPage;
