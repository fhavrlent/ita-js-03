import React, { Component } from 'react';
import Details from '../components/Details/Details';

class DetailsPage extends Component {

  render() {
    return (
      <div>
        <Details />
      </div>
    );
  }
}

export default DetailsPage;
