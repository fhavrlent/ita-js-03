import React, { Component } from 'react';
import { Grid } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar/AppNavbar';

class AppPage extends Component {
  render() {
    return (
      <div>
        <AppNavbar />
        <Grid>
          {this.props.children}
        </Grid>
      </div>
    );
  }
}

export default AppPage;
