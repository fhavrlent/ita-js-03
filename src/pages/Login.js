import React, { Component } from 'react';
import Login from '../components/Login/Login';

class LoginPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      loggedIn: false,
    }
    this.handleLogin = this.handleLogin.bind(this);
  }

  

  handleLogin(event){
    event.preventDefault();
    if(!this.state.loggedIn){
      this.setState({
        loggedIn: true,
      })
    } else {
      this.setState({
        loggedIn: false,
      })
    }
  }

  render() {
    return (
      <div>
        <Login
          loggedIn={this.state.loggedIn}
          handleLogin={this.handleLogin}
        />
      </div>
    );
  }
}

export default LoginPage;
