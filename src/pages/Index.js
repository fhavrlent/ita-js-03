import React from 'react';
import Index from '../components/Index/Index';

class IndexPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      users: [
        {
          id: 1,
          name: 'John Doe',
          phone: '0902333666',
          address: 'Dolna Horna 55',
          note: 'Plesaty'
        },
        {
          id: 2,
          name: 'John Poe',
          phone: '0902333666',
          address: 'Dolna Horna 56',
          note: 'Hluchy'
        },
        {
          id: 3,
          name: 'John Toe',
          phone: '0902333666',
          address: 'Dolna Horna 33',
          note: 'Skaredy'
        },
        {
          id: 4,
          name: 'John Lel',
          phone: '0902333666',
          address: 'Dolna Horna 33',
          note: 'Skaredy'
        }
      ]
    }
  }

  render() {
    const { users } = this.state;
    return (
      <div>
        <Index users={users}/>
      </div>
    )
  }
}

export default IndexPage;
