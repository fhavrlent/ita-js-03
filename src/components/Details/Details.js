import React from 'react';


function Details() {
  return (
    <div className="container">
      <h2>Contact detail</h2>
      <div className="row">
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-4">Name</div>
            <div className="col-md-8">John Doe</div>
          </div>
          <div className="row">
            <div className="col-md-4">Phone</div>
            <div className="col-md-8">...</div>
          </div>
          <div className="row">
            <div className="col-md-4">Address</div>
            <div className="col-md-8">...</div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-4">Note</div>
            <div className="col-md-8">...</div>
          </div>
        </div>
      </div>
      <div>
        <a href="" className="btn btn-default">Edit</a>
        <a href="" className="btn btn-default">Delete</a>
      </div>
    </div>
  );
}

export default Details;

