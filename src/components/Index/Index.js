import React from 'react';
import TableEntry from './TableEntry'

function Index({users}) {
  return (
    <div className="container">
      <h2>Contacts</h2>
      <div className="row">
        <div className="col-md-4">
          <button className="btn btn-default">New Contact</button>
        </div>
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <input type="text" className="form-control" placeholder="Search..." />
        </div>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Note</th>
          </tr>
        </thead>
        <tbody>
          {users.map(({id, name, phone, address, note}) => (
            <TableEntry key={id} id={id} name={name} phone={phone} address={address} note={note} />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Index;
