import React  from 'react';
import UsernameForm from './UsernameForm';
import PasswordForm from './PasswordForm';
import SubmitButton from '../Common/SubmitButton';

function Login({
  loggedIn,
  handleLogin,
}) {
  if(!loggedIn){
    return (
      <div className='col-md-4 col-md-offset-4'>
        <form onSubmit={handleLogin}>
          <UsernameForm  />
          <PasswordForm />
          <SubmitButton text='Login'/>
        </form>
      </div>
    );
  } else {
    return (
      <div>
        <button type='button' onClick={handleLogin} className='btn btn-success'>Logout</button>
      </div>
    )
  }
}

export default Login;
