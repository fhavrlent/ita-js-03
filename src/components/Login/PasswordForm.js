import React  from 'react';
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap';

function PasswordForm () {
  return (
    <FormGroup controlId='password'>
      <ControlLabel>Password: </ControlLabel>
      <FormControl
        type='password'
        name='password'
      />
    </FormGroup>
  );
}

export default PasswordForm;
